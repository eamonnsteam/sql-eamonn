Q1-------
SELECT * FROM bond WHERE [CUSIP] = '28717RH95'
Q2-------
select * from bond ORDER BY maturity asc
Q3-------
SELECT SUM(quantity*price) as total from bond 
Q4-------
SELECT b.CUSIP, Year(b.maturity),SUM((b.coupon / 100 ) * b.quantity) as returnAnnual from bond b GROUP BY Year(b.maturity),b.CUSIP ORDER BY Year(b.maturity) asc
Q5-------
SELECT * FROM bond b  JOIN rating r on b.rating = r.rating WHERE r.ordinal <=3
Q6-------
SELECT avg(b.price) as averagePrice, avg(b.coupon) as averageCoupon, b.rating as itemRating  FROM bond b JOIN rating r on b.rating = r.rating GROUP BY b.rating
Q7-------
SELECT b.CUSIP, r.Rating as yieldRating FROM bond b join rating r on b.rating = r.rating WHERE b.coupon/b.price < r.expected_yield


SELECT * FROM trader t join position p ON t.ID = p.trader_ID join trade tr ON p.opening_trade_ID = tr.ID ORDER BY tr.stock



--Total Buy View
CREATE VIEW traderViewBuy as
SELECT        SUM(tr.price * tr.size) AS totalBuy
FROM            dbo.trader AS t INNER JOIN
                         dbo.position AS p ON t.ID = p.trader_ID INNER JOIN
                         dbo.trade AS tr ON p.opening_trade_ID = tr.ID
WHERE        (tr.buy = '1')

--Total Sell Views
CREATE VIEW traderViewSell as
SELECT        SUM(tr.price * tr.size) AS totalSell
FROM            dbo.trader AS t INNER JOIN
                         dbo.position AS p ON t.ID = p.trader_ID INNER JOIN
                         dbo.trade AS tr ON p.opening_trade_ID = tr.ID
WHERE        (tr.buy = '0')

--Total Sell
CREATE VIEW dailyPL as
SELECT   dbo.traderViewSell.totalSell - dbo.traderViewBuy.totalBuy as profitloss FROM    dbo.traderViewBuy CROSS JOIN dbo.traderViewSell



-- Trader Buy Amount
CREATE VIEW traderBuys as
SELECT        t.ID, t.first_name, t.last_name, tr.size * tr.price AS traderBuy
FROM            dbo.trader AS t INNER JOIN
                         dbo.position AS p ON t.ID = p.trader_ID INNER JOIN
                         dbo.trade AS tr ON p.opening_trade_ID = tr.ID
WHERE        (tr.buy = '1')
--Trader Sells Amount
CREATE VIEW traderSells as
SELECT        t.ID, t.first_name, t.last_name, tr.size * tr.price AS traderSell
FROM            dbo.trader AS t INNER JOIN
                         dbo.position AS p ON t.ID = p.trader_ID INNER JOIN
                         dbo.trade AS tr ON p.opening_trade_ID = tr.ID
WHERE        (tr.buy = '0')
--Trader Profit/Loss
CREATE VIEW traderProfitLoss As
SELECT        dbo.traderBuys.first_name, dbo.traderBuys.last_name, SUM(dbo.traderBuys.traderBuy - dbo.traderSells.traderSell) AS profitorloss
FROM            dbo.traderBuys INNER JOIN
                         dbo.traderSells ON dbo.traderBuys.ID = dbo.traderSells.ID
GROUP BY dbo.traderBuys.first_name, dbo.traderBuys.last_name
